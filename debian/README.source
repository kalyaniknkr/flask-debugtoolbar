This package is maintained with git-buildpackage(1). It follows DEP-14 for
branch naming (e.g. using debian/master for the current version in Debian
unstable due Debian Python team policy).

It uses pristine-tar(1) to store enough information in git to generate bit
identical tarballs when building the package without having downloaded an
upstream tarball first.

When working with patches it is recommended to use "gbp pq import" to import
the patches, modify the source and then use "gbp pq export --commit" to commit
the modifications.

The changelog is generated using "gbp dch" so if you submit any changes don't
bother to add changelog entries but rather provide a nice git commit message
that can then end up in the changelog.

It is recommended to build the package with pbuilder using:

    gbp buildpackage --git-pbuilder

For information on how to set up a pbuilder environment see the git-pbuilder(1)
manpage. In short:

    DIST=sid git-pbuilder create
    gbp clone https://salsa.debian.org/python-team/packages/flask-debugtoolbar.git
    cd flask-debugtoolbar
    gbp buildpackage --git-pbuilder


The original upstream source contains some source files which are minimized.
As there are equivalent packaged versions available in the archive the
files and folder in question are getting removed while importing the source
and get replaced with symlinks to the data from the packages at build time.
Have also a look at the copyright file to which files/folders are removed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 7 Mar 2024 20:30:00 +0100
